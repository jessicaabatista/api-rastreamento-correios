<?php
session_start();
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<title>API Rastreamento</title>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<meta name="google-signin-client_id" content="299120802998-evcs4l967da28bb128oki364hfuasjvd.apps.googleusercontent.com">
</head>

<body>
	Bem vindo <?php echo $_SESSION['userName']; ?>!

	<a href="login.php" onclick="signOut();">Sair</a>
	<script>
		function signOut() {
			var auth2 = gapi.auth2.getAuthInstance();
			auth2.signOut().then(function() {
				console.log('User signed out.');
			});
		}
	</script>

	</br>

	<form action="rastreio.php" method="POST">

		<fieldset>
			<legend>Rastreamento via Correios</legend>

			<p>
				<label for="nome">Código: </label>
				<input type="text" name="codigo" id="codigo">
			</p>

		</fieldset>

		<p>
			<input type="submit" value="Enviar" id="enviar">
		</p>

	</form>

</body>

</html>